import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {makeStateKey, TransferState} from "@angular/platform-browser";

@Component({
  selector: 'home',
  template: `
      <h3>{{ message }}</h3>
      <ul *ngIf="result">
          <li *ngFor="let feature of result.features">
              {{ feature.properties.city }} - {{ feature.properties.label }} - {{ feature.properties.postcode }} - {{ feature.properties.type }}
          </li>
      </ul>
  `
})
export class HomeComponent implements OnInit {
  public message: string;
  public result: any;

  constructor(private http: HttpClient, private transferState: TransferState) {}

  ngOnInit() {
    this.message = 'Hello';

      let myTransferStateKey = makeStateKey<any>("myDatas");

      if(this.transferState.hasKey(myTransferStateKey)) {
          this.result = this.transferState.get(myTransferStateKey, {});
          this.transferState.remove(myTransferStateKey);
      } else {
          this.http.get("https://api-adresse.data.gouv.fr/search/?q=seclin").subscribe(response => {
              this.result = response;
              this.transferState.set(myTransferStateKey, this.result);
          });
      }


  }
}